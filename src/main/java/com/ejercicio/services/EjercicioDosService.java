package com.ejercicio.services;

import com.ejercicio.constants.EjercicioDosConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

@Service
public class EjercicioDosService implements EjercicioDosConstants {

    static Logger log = LogManager.getLogger(EjercicioDosService.class);

    public Map<String, Object> calculateHour(LocalTime time, Integer timeZone) {
        Map<String, Object> mapResult = new HashMap<String, Object>();
        try {
            LocalTime localTime = null;
            if (timeZone < 0) {
                localTime = time.plusHours(timeZone * -1);
            } else {
                localTime = time.minusHours(timeZone);
            }
            log.info("Consulta exitosa");
            mapResult.put(TIME, localTime);
            mapResult.put(TIME_ZONE, UTC);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Se prudujo un error: " + e.getMessage());
            mapResult.put(TYPE, MESSAGE_TYPE_ERROR);
            mapResult.put(MESSAGE, MESSAGE_ERROR);
        }

        return mapResult;
    }
}
