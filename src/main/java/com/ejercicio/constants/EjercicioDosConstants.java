package com.ejercicio.constants;

public interface EjercicioDosConstants {

    String CONTENT_TYPE                                 = "application/json";
    String ACCEPT                                       = "application/json";

    Integer ZERO                                        = 0;

    String TIME                                         = "time";
    String TIME_ZONE                                    = "timeZone";
    String FORMAT_HOUR                                  = "HH:mm:ss";
    String UTC                                          = "utc";
    String RESPONSE                                     = "response";

    String TYPE                                         = "type";
    String MESSAGE_TYPE_ERROR                           = "Error";
    String MESSAGE_TYPE_SUCCESS                         = "Succes";
    String MESSAGE                                      = "message";
    String DATA                                         = "data";
    String MESSAGE_ERROR                                = "Ha ocurrido un error, verifique.";
    String MESSAGE_SUCCESS                              = "Ejecución de servicio exitosa.";
    String MESSAGE_EMPTY_ELEMENT                        = "Parametros de entrada nulos, verifique.";

    String CALLTIME                                     = "/calltime";
    String SAVE_CANDIDATE_DESCRIPTION                   = "<div class='alert alert-info'><h3><u><span class='label label-default'> Guardar Candidato " +
                                                        "</span></u></h3>"
                                                        + "Método que guarda un candidato. Recibe los siguientes parámetros:"
                                                        + "<ul class='list-group'>"
                                                        + "<li class='list-group-item list-group-item-warning'><b><u>name:</u></b> Hora.  </li></ul>"
                                                        + "<li class='list-group-item list-group-item-warning'><b><u>lastName:</u></b> TimeZone.  </li></ul>"
                                                        + "<u>Ejemplo:</u>"
                                                        + "<pre>{<br />"
                                                        + "\t\"<b>time</b>\": <em>\"18:31:45\"</em>,<br />"
                                                        + "\t\"<b>timeZone</b>\": <em>-3</em><br />"
                                                        + "}</pre>"
                                                        + "En caso de enviar los parámetros correctamente, deberá recibir un mapa en formato json de esta forma:"
                                                        + "<ul class='list-group'><li class='list-group-item list-group-item-success'><b><u>message:</u></b> Un mensaje para mostrar. </li></ul> <br />"
                                                        + "<br /><span class='badge'>Nota:</span> <br />En el json string, cada clave deberá estar entre comillas dobles <kbd>\"</kbd>; el uso de comillas"
                                                        + " dobles para el valor será opcional a excepción de valores tipo string.</div>";
}
