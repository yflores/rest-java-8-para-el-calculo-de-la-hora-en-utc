package com.ejercicio.controller;

import com.ejercicio.constants.EjercicioDosConstants;
import com.ejercicio.services.EjercicioDosService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

//@ApiVersion(since = "1.0")
@RestController
@RequestMapping("/hour")
@Api(value = "Servicio", description = "Calculo de Hora por TimeZone")
public class EjercicioDosController implements EjercicioDosConstants {

    static Logger log = LogManager.getLogger(EjercicioDosController.class);

    //Servicios//
    @Autowired
    EjercicioDosService ejercicioDosService;

    @PostMapping(value = CALLTIME, produces = ACCEPT)
    public Object callTime(@RequestBody String json) {
        Map<String, Object> mapResponse = new HashMap<String, Object>();

        if (json != null && json.length() > ZERO) {
            try {
                Map<String, Object> params = new ObjectMapper().readerFor(Map.class).readValue(json);

                LocalTime time = (params.containsKey(TIME) && params.get(TIME) != null && !params.get(TIME).toString().isEmpty()) ? LocalTime.parse(params.get(TIME).toString(), DateTimeFormatter.ofPattern(FORMAT_HOUR)) : null;
                Integer timeZone = (params.containsKey(TIME_ZONE) && params.get(TIME_ZONE) != null) ? Integer.parseInt(params.get(TIME_ZONE).toString()) : null;

                if (time != null && timeZone != null) {
                    mapResponse.put(RESPONSE, ejercicioDosService.calculateHour(time, timeZone));
                } else {
                    log.error("Se prudujo un error: " + MESSAGE_EMPTY_ELEMENT);
                    mapResponse.put(TYPE, MESSAGE_TYPE_ERROR);
                    mapResponse.put(MESSAGE, MESSAGE_EMPTY_ELEMENT);
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Se prudujo un error: " + e.getMessage());
                mapResponse.put(TYPE, MESSAGE_TYPE_ERROR);
                mapResponse.put(MESSAGE, MESSAGE_ERROR);
                return mapResponse;
            }
        }
        return mapResponse;
    }

 /*   static Logger log = LogManager.getLogger(EjercicioDosController.class);

    //Servicios//
    @Autowired
    EjercicioDosService ejercicioDosService;

    @ApiMethod(consumes = CONTENT_TYPE, produces = ACCEPT, description = SAVE_CANDIDATE_DESCRIPTION)
    @RequestMapping(method = RequestMethod.POST, value = CALLTIME)
    @ApiResponseObject
    @ResponseBody
    public Object callTime(HttpServletRequest request, HttpServletResponse response,
                           @ApiBodyObject(clazz = String.class) @RequestBody String json) {
        Map<String, Object> mapResponse = new HashMap<String, Object>();

        if (json != null && json.length() > ZERO) {
            try {
                Map<String, Object> params = new ObjectMapper().readerFor(Map.class).readValue(json);

                LocalTime time = (params.containsKey(TIME) && params.get(TIME) != null && !params.get(TIME).toString().isEmpty()) ? LocalTime.parse(params.get(TIME).toString(), DateTimeFormatter.ISO_LOCAL_DATE) : null;
                Integer timeZone = (params.containsKey(TIME_ZONE) && params.get(TIME_ZONE) != null) ? Integer.parseInt(params.get(TIME_ZONE).toString()) : null;

                if (time != null && timeZone != null) {
                    mapResponse = ejercicioDosService.calculateHour(time, timeZone);
                } else {
                    log.error("Se prudujo un error: " + MESSAGE_EMPTY_ELEMENT);
                    mapResponse.put(TYPE, MESSAGE_TYPE_ERROR);
                    mapResponse.put(MESSAGE, MESSAGE_EMPTY_ELEMENT);
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Se prudujo un error: " + e.getMessage());
                mapResponse.put(TYPE, MESSAGE_TYPE_ERROR);
                mapResponse.put(MESSAGE, MESSAGE_ERROR);
                return mapResponse;
            }
        }
        return mapResponse;
    }*/
}
