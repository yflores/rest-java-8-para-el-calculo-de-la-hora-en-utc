El proyecto fue desarrollado en Java Spring Boot JPA, fue subido a GitLab para versionarlo allí, para clonar seria:

**SSH:** git@gitlab.com:yflores/rest-java-8-para-el-calculo-de-la-hora-en-utc.git

**HTTPS:** https://gitlab.com/yflores/rest-java-8-para-el-calculo-de-la-hora-en-utc.git

Hacer desde una terminal el comando: **git clone (SSH / HTTPS)**.

Luego de bajar el proyecto se le debe aplicar instalar en maven, desde terminal: mvn install para que de esta forma instale todas las dependencias, luego desde el IDE se procede a ejecutar el proyecto, en mi caso estoy usando Intellij.

Además por efectos del proyecto se necesita una base de datos, lo configure ara que fuera MySQL llamada rrhh de usuario proyecto y contraseña 123456, por lo que debe ejecutarse el scripts:

**mysql>** CREATE DATABASE rrhh CHARACTER SET utf8 COLLATE utf8_general_ci;

**mysql>** CREATE USER 'proyecto'@'localhost' identified by '123456';

**mysql>** GRANT ALL PRIVILEGES ON rrhh.* TO proyecto@localhost;

**mysql>** FLUSH PRIVILEGES;

Al tener el proyecto ya en ejecución desde el navegador: **localhost:8082/swagger-ui.html** esto es para ver la documentación del servicio desarrollado. El servicio recibirá como entrada un json de esta forma:

**{"time": "18:00:00", "timeZone": -3}**
